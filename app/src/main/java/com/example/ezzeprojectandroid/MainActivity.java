package com.example.ezzeprojectandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    ListView l;

    Spinner spinner;
    //Spinner spinner2;
    List<String> usersList;
    List<String> todoList;
    ArrayAdapter<String> catAdapter;
    ArrayAdapter<String> todoAdapter;
    String selected_val;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner)findViewById(R.id.spinner);
        //spinner2 = (Spinner)findViewById(R.id.spinner2);
        usersList = new ArrayList<String>();
        todoList = new ArrayList<String>();

        catAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, usersList);
        todoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, todoList);
        spinner.setAdapter(catAdapter);
        //spinner2.setAdapter(todoAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?>arg0, View view, int arg2, long arg3) {

                String uid=spinner.getSelectedItem().toString();

                String selected_val = uid.replaceAll("[^0-9]", " ");

                selected_val  = selected_val.trim();

                Toast.makeText(getApplicationContext(), selected_val ,
                        Toast.LENGTH_SHORT).show();

                l = findViewById(R.id.list);
                l.setAdapter(todoAdapter);

                getHttpResponse2(selected_val);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });




        getHttpResponse();


    }

    public void getHttpResponse() {

        String url = "https://jsonplaceholder.typicode.com/users";

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String mMessage = response.body().string();

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Response", mMessage);
                        try {

                            JSONArray usersArr = new JSONArray(mMessage);

                            for(int i =0; i < usersArr.length(); i++){
                                JSONObject user = usersArr.getJSONObject(i);
                                Log.e("user", user.getString("username"));

                                String uname = user.getString("username");
                                String uid = user.getString("id");
                                /*usersList.add(user.getString("username"));
                                usersList.add(user.getString("id"));*/
                                usersList.add(uname + " -- " +  uid);

                            }

                            catAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
    }

    public void getHttpResponse2(String selected_val) {

        String url = "https://jsonplaceholder.typicode.com/todos?userId="+selected_val;

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String mMessage = response.body().string();

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Response", mMessage);
                        try {

                            todoList.clear();

                            JSONArray usersArr = new JSONArray(mMessage);

                            for(int i =0; i < usersArr.length(); i++){
                                JSONObject user = usersArr.getJSONObject(i);
                                String uid = user.getString("userId");
                                Log.e("user", user.getString("userId"));
                                Log.e("user", user.getString("userId"));
                                Log.e("user", user.getString("title"));
                                todoList.add(user.getString("id"));
                                todoList.add(user.getString("userId"));
                                todoList.add(user.getString("title"));
                                todoList.add(user.getString("completed"));
                            }
                            todoAdapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
    }
}